/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2018 Pedro Henrique Lara Campos
 */

#include <coffe/utils.h>
#include <coffe/cgi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <linux/limits.h>

int main() {
	kc_fixpath();
	
	char *parameters = getenv("QUERY_STRING");
	if(parameters == NULL)
		return kccgi_diebyparams();

	char nick_esc[256], *nick;

	if(sscanf(parameters, "nick=%s", &nick_esc) != 1)
		return kccgi_diebyparams();
	
	nick = (char*) malloc(strlen(nick_esc)+1);
	urldecode2(nick, nick_esc);

	char *z_max=nick+strlen(nick), *z;
	for(z=nick; z<z_max; z++)
		if(*z == '"')
			*z='\'';

	time_t timestamp;
    struct tm* now;

    time(&timestamp);
    now = localtime(&timestamp);

	char path[PATH_MAX];
	strftime(path, PATH_MAX, CF_PERDAY, now);
	mkdir_p(path);

	strftime(path, PATH_MAX, CF_ENTRY, now);
	FILE *entry = fopen(path, KC_F_W);
	
	if(entry) {
		fprintf(entry, "s%s", nick);
		fclose(entry);
		printf(CGI_PLAINTEXT "Status: 200 OK\r\n\r\n");
	} else
		kccgi_internalerror();

	free(nick);
	return 0;
}