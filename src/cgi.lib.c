/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2018 Pedro Henrique Lara Campos
 */

#include <coffe/cgi.h>
#include <stdio.h>
#include <string.h>

const static char _KCCGI_400[] = CGI_PLAINTEXT "Status: 400 Bad Request\r\n\r\nDesculpe, os parametros enviados são inválidos!\r\n";
const static char _KCCGI_401[] = CGI_PLAINTEXT "Status: 401 Unauthorized\r\n\r\nDesculpe, mas suas credenciais estão inválidas!\r\n";
const static char _KCCGI_500[] = CGI_PLAINTEXT "Status: 500 Internal Server Error\r\n\r\nErro inesperado!\r\n";

int kccgi_diebyparams() {
	fwrite(_KCCGI_400,sizeof(_KCCGI_400)-1, 1, stdout);
	return 0;
}

int kccgi_diebyinvalidpass() {
	fwrite(_KCCGI_401,sizeof(_KCCGI_401)-1, 1, stdout);
	return 0;
}

int kccgi_internalerror() {
	fwrite(_KCCGI_500,sizeof(_KCCGI_500)-1, 1, stdout);
	return 0;
}