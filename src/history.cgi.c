/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2018 Pedro Henrique Lara Campos
 */

#include <coffe/utils.h>
#include <coffe/cgi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <linux/limits.h>

const char _KCJSON_H0[] = "Content-Type: application/json\r\n\r\n";

int main() {
	kc_fixpath();
	
	fwrite(_KCJSON_H0,sizeof(_KCJSON_H0)-1, 1, stdout);
	printf("{\"entries\":[");

	time_t timestamp;
	struct tm* now;

	time(&timestamp);
	now = localtime(&timestamp);

	char dir_path[PATH_MAX], entry_path[PATH_MAX];
	strftime(dir_path, PATH_MAX, CF_PERDAY, now);
	
	struct dirent *dir;
	DIR *d = opendir(dir_path);
	bool z = false;
	if (d) {
		while ((dir = readdir(d)) != NULL) {
			if(*dir->d_name == '.')
				continue;
			if(z)
				printf(",");
			sprintf(entry_path, "%s/%s", dir_path, dir->d_name);
			FILE *f = fopen(entry_path, KC_F_R);
			if(!f)
				continue;
			printf("{\"time\":\"%s\"", dir->d_name);
			char op, nick[256];
			fread(&op, 1, 1, f);
			switch(op) {
				case 'r':
					fread(&op, 1, 1, f);
					fscanf(f, "%s", nick);
					printf(",\"action\":\"rate\",\"grade\":\"%c\",\"nick\":\"%s\"",op,nick);
					break;
				case 'd':
					fscanf(f, "%s", nick);
					printf(",\"action\":\"done\",\"nick\":\"%s\"",nick);
					break;
				case 'e':
					fscanf(f, "%s", nick);
					printf(",\"action\":\"empty\",\"nick\":\"%s\"",nick);
					break;
				case 's':
					fscanf(f, "%s", nick);
					printf(",\"action\":\"nosugar\",\"nick\":\"%s\"",nick);
					break;
			}
			printf("}");
			z = true;
			fclose(f);
		}
		closedir(d);
	}


	printf("]}\n");
	
	return 0;
}