/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2018 Pedro Henrique Lara Campos
 */

#include <coffe/utils.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <linux/limits.h>


const char *KC_F_W = "wb", *KC_F_R = "rb", *KC_F_A = "ab";

const char *CF_PERDAY="run/coffe/%Y-%m-%d",
	*CF_ENTRY="run/coffe/%Y-%m-%d/%H-%M-%S";

#define read_opts(in, out, fname, fdir, f, fsz) \
	sprintf(fname, in, fdir); \
	f = fopen(fname, KC_F_R); \
	if(f) { \
		fsz = ftell(f); \
		rewind(f); \
		out = (char *) malloc(fsz+1); \
		out[fsz] = 0; \
		fread(out, sizeof(char), fsz, f); \
		fclose(f); \
	} else \
		out = NULL; \

void mkdir_p(char *dir) {
	char tmp[PATH_MAX];
	char *p = NULL;
	size_t len;

	snprintf(tmp, sizeof(tmp),"%s",dir);
	len = strlen(tmp);
	if(tmp[len - 1] == '/')
		tmp[len - 1] = 0;
	for(p = tmp + 1; *p; p++)
		if(*p == '/') {
			*p = 0;
			mkdir(tmp, S_IRWXU);
			*p = '/';
		}
	mkdir(tmp, S_IRWXU);
}

void urldecode2(char *dst, const char *src) {
	// TAKEN FROM: http://stackoverflow.com/posts/14530993
	char a, b;
	while (*src) {
		if ((*src == '%') &&
		    ((a = src[1]) && (b = src[2])) &&
		    (isxdigit(a) && isxdigit(b))) {
			if (a >= 'a')
				a -= 'a'-'A';
			if (a >= 'A')
				a -= ('A' - 10);
			else
				a -= '0';
			if (b >= 'a')
				b -= 'a'-'A';
			if (b >= 'A')
				b -= ('A' - 10);
			else
				b -= '0';
			*dst++ = 16*a+b;
			src+=3;
		} else if (*src == '+') {
			*dst++ = ' ';
			src++;
		} else {
			*dst++ = *src++;
		}
	}
	*dst++ = '\0';
}

void kc_fixpath() {
	char fname[PATH_MAX], *run_path, def_run_path[]=RUN_PATH;
	run_path = getenv("RUN_PATH");
	if(!run_path)
		run_path = def_run_path;

	if(run_path[0] == '/') {
		//puts(run_path);
		chdir(run_path);
	}

	else {
		readlink("/proc/self/exe", fname, PATH_MAX-1);
		char *lp = strrchr(fname, '/');
		if(lp != NULL)
			*lp = 0;
		strcat(fname, "/");
		strcat(fname, run_path);
		//puts(fname);
		chdir(fname);
	}
}

void kc_cat(char *fname, FILE *out) {
	fflush(out);
	int f = open(fname, O_RDONLY);
	struct stat fileinfo = {0};
	fstat(f, &fileinfo);
	sendfile(fileno(out), f, NULL, fileinfo.st_size);
	//splice(f, NULL, fileno(out), 0);
	close(f);
}

bool kc_fcat(char *fname, FILE *stream, ...) {
	va_list vals;
	va_start(vals, stream);
	FILE *f = fopen(fname, KC_F_R);
	if(!f)
		return false;
	char k[4096], *l, *m, *p;
	k[4095] = 0;
	while(fgets(k, sizeof(k)-1, f) != NULL) {
		m = l = k;
		while((m = strchr(l, 1)) != 0) {
			*m = 0;
			fputs(l, stream);
			p = va_arg(vals, char*);
			if(p)
				fputs(p, stream);
			l = m+1;
		}
		fputs(l, stream);
	}
	fclose(f);
	va_end(vals);
	return true;
}