/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2018 Pedro Henrique Lara Campos
 */

#ifndef _COFFE_UTILS_H
#define _COFFE_UTILS_H	1

#include <stdbool.h>
#include <stdio.h>

const char *KC_F_W, *KC_F_R, *KC_F_A;
const char *CF_PERDAY, *CF_ENTRY;

void mkdir_p(char *dir);
void urldecode2(char *dst, const char *src);

void kc_fixpath();
void kc_cat(char *fname, FILE *out);
bool kc_fcat(char *fname, FILE *stream, ...);


#endif