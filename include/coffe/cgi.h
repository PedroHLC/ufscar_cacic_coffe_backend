/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2018 Pedro Henrique Lara Campos
 */

#ifndef _KCOFFE_CGI_H

# define _KCOFFE_CGI_H	1

#include <stdio.h>

int kccgi_diebyparams();
int kccgi_diebyinvalidpass();
int kccgi_internalerror();

#define CGI_PLAINTEXT "Content-type: text/plain; charset=UTF-8\r\n"

#endif