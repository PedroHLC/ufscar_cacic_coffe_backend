RUN_PATH?=\"$HOME/AndroidStudioProjects/cacic_coffe/backend\"
CFLAGS=-Wno-discarded-qualifiers -D RUN_PATH="${RUN_PATH}" -g -I./include -L./lib
LINK_LIBS=-lcoffe_utils -lcoffe_cgi

CC=gcc
BUNDLE_AR=ar rcs

LS_LIBS=utils cgi
LS_PAGES=done rate empty history nosugar

INSTALL_PATH?=${HOME}/public_html

all: libs pages

build/%.o: src/%.lib.c
	${CC} -o $@ -c $< ${CFLAGS}

lib/libcoffe_%.a: build/%.o
	${BUNDLE_AR} $@ $<

libs: $(foreach f, $(LS_LIBS), lib/libcoffe_${f}.a)

bin/coffe_%.cgi: src/%.cgi.c libs
	${CC} -o $@ $< ${LINK_LIBS} ${CFLAGS}
	strip $@

pages: $(foreach f, $(LS_PAGES), bin/coffe_${f}.cgi)

.PHONY: clean install

install:
	mkdir -p ${INSTALL_PATH}/cgi-bin
	$(foreach f, $(LS_PAGES), cp bin/coffe_${f}.cgi ${INSTALL_PATH}/cgi-bin/;)

clean:
	$(foreach f, $(LS_LIBS), rm lib/libcoffe_${f}.a;)
	$(foreach f, $(LS_PAGES), rm bin/coffe_${f}.cgi;)